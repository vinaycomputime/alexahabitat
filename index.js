/* eslint-disable no-console */
const AWS = require("aws-sdk");
//vinay
const https = require('https');
//vinay
var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
//var documentClient = new AWS.DynamoDB.DocumentClient();

const GET_DEVICES_BY_USER_FUNCTION = process.env.GET_DEVICES_BY_USER_FUNCTION;
const VERIFY_COGNITO_TOKEN_FUNCTION = process.env.VERIFY_COGNITO_TOKEN_FUNCTION;
const IOT_ENDPOINT = process.env.IOT_ENDPOINT;
const DEVICE_TABLE_NAME = process.env.DEVICE_TABLE;
const MODEL_TABLE_NAME = process.env.MODEL_TABLE;
const USE_PHYSICAL_DEVICE = process.env.USE_PHYSICAL_DEVICE;
const IOT_REGION = process.env.REGION;

const AlexaResponse = require("./AlexaResponse");
const discoveryConfig = require ("./discoveryConfig");
//var iot = new AWS.Iot();
//var iotdata = new AWS.IotData({ endpoint: IOT_ENDPOINT });
var iot;
var iotdata;
const dynamodb = new AWS.DynamoDB.DocumentClient();
const lambda = new AWS.Lambda();
const REQUIRED_PAYLOAD_VERSION = "3";
//vinay
var accessToken;
var flag = 1;  //vinay

/*
    TODO: 
    
    1. Consider re-writing to store endpoint-to-user mapping as an attribute
       in IoT registry rather than DynamoDB; eliminate need for DDB? 

    2. Consider storing attributes needed for discovery, e.g. manufacturer name, 
       as attributes in IoT core, instead of a Lambda config file? 
         - on second thought, the complex nested structure of things like the
           Alexa capabilities means simple IoT attributes probably wouldn't
           suffice. DynamoDB comes to mind, but it's easier to store this info
           in a config file rather than an item in a DDB database. So, maybe
           makes sense to keep it where it is (in discoveryConfig.js)
*/

// For debugging / test purposes if you do not have a physical device linked to
// your Thing in the IoT Core Registry, set the value below to true; this will
// copy your desired state changes to the reported state changes. Normally, 
// only the physical device would update reported state. If using a physical device,
// set the value below to false:
const copyDesiredStateToReportedStateInShadow = !(USE_PHYSICAL_DEVICE);

exports.handler = async function (request, context) {

    try {
        
        AWS.config.region = IOT_REGION;
        
        iot = new AWS.Iot();
        iotdata = new AWS.IotData({ endpoint: IOT_ENDPOINT });
        
        log("Alexa request:\n", request); //vinay
        //log("Alexa context:\n", context); //vinay
		/*var paramsThing = {
		"thingGroupName": 'Gateway-001E5E02C514',
		"maxResults": 2
		};
		var thng = await iot.listThingsInThingGroup(paramsThing).promise();
		console.log(thng)
		        let params = {
                thingName: thng.things[1]
        };
        var iotDescrip = await iot.describeThing(params).promise();
        console.log(iotDescrip)
        //console.log(iotDescrip.attributes)
        */

        verifyRequestContainsDirective(request);
        
        var directive = request.directive;
        var header = directive.header;
        var namespace = header.namespace; 
        var name = header.name;
        var ignoreExpiredToken = request.ignoreExpiredToken || false;   // True for debugging purposes, if we're using an old token

        verifyPayloadVersionIsSupported(header.payloadVersion);
        
        var userId = await verifyAuthTokenAndGetUserId(
            namespace,
            directive,
            ignoreExpiredToken
        );

        console.log(request.directive); //vinay
            
        // Is this a request to discover available devices?
        //vinay: debug
        //namespace = 'Alexa.Discovery'
        if (namespace === 'Alexa.Discovery') {
            //vinay
            if (name === 'AddOrUpdateReport') {
            console.log('Received Update request.........');
            }
            var response = await handleDiscovery(request, context, userId);
            return sendResponse(response.get());
        }
        else if (namespace === 'Alexa.Authorization') {
            var response = await handleAuthorization(request, context, userId);
            return sendResponse(response.get());
        }
        // If this is not a discovery request, it is a directive to do something
        // to or retrieve info about a specific endpoint. We must verify that
        // the endpoint exists * and * is mapped to our current user before 
        // we do anything with it: 
        else {
           
            var endpoint = await verifyEndpointAndGetEndpointDetail(userId, directive.endpoint.endpointId);
    
            verifyEndpointConnectivity(endpoint);

            if (namespace === 'Alexa.ThermostatController') {
                let response = await handleThermostatControl(request, context, endpoint);
                return sendResponse(response.get());
            }
            else if (namespace === 'Alexa' && name === 'ReportState') {
                let response = await handleReportState(request, context, endpoint);
                //setTimeout(function() {return sendResponse(response.get())}, 1000);
                return sendResponse(response.get());
                //return sendResponse(response);    //vinay
            }
            else {       
                throw new AlexaException(
                    'INVALID_DIRECTIVE', 
                    `Namespace ${namespace} with name ${name} is unsupported by this skill.`
                );
            }
        }
    }
    catch (err) {
        log(`Error: ${err.name}: ${err.message}`);
        log('Stack:\n' + err.stack);
        var errorType, errorMessage, additionalPayload;

        // if AlexaError key is present (regardless of value), then we threw
        // an error intentionally and we want to bubble up a specific Alexa
        // error type. If the key is not present, it is an unhandled error and
        // we respond with a generic Alexa INTERNAL_ERROR message.
        if (err.hasOwnProperty('AlexaError')) {
            errorType = err.name;
            errorMessage = err.message;
            additionalPayload = checkValue(err.additionalPayload, undefined);
        } else {
            errorType = 'INTERNAL_ERROR';
            errorMessage = `Unhandled error: ${err}`
        }
        return sendErrorResponse(errorType, errorMessage, additionalPayload);
    }

};

function verifyEndpointConnectivity(endpoint) {

    //log('Verifying endpoint is online...');   //vinay
    var shadow = endpoint.shadow;
    //log(shadow) //vinay
    
    if (shadow.hasOwnProperty('state') === false) {
        throw new AlexaException('ENDPOINT_UNREACHABLE', 'Shadow does not contain a state object');
    }

    if (shadow.state.hasOwnProperty('reported') === false) {
        throw new AlexaException('ENDPOINT_UNREACHABLE', 'Shadow does not contain a state.reported object');
    }
    
    //PTAC - commented
    /*if (shadow.state.reported.hasOwnProperty('connected') === false) {
        throw new AlexaException('ENDPOINT_UNREACHABLE', 'Shadow does not contain a state.reported.connectivity object');
    }
    true
    if (shadow.state.reported.connected !== '') {
        throw new AlexaException('ENDPOINT_UNREACHABLE', `Device unavailable, reported online=${shadow.state.reported.connected}`);
    }*/

    log('Endpoint is online.'); //vinay

}

function verifyRequestContainsDirective(request) {
    // Basic error checking to confirm request matches expected format.
    if (!('directive' in request)) {
        throw new AlexaException(
            'INVALID_DIRECTIVE',
            'directive is missing from request'
        );
    }
}

function verifyPayloadVersionIsSupported(payloadVersion) {
    // Basic error checking to confirm request matches expected format.
    if (payloadVersion !== REQUIRED_PAYLOAD_VERSION) {
        throw new AlexaException(
            'INVALID_DIRECTIVE', 
            `Payload version ${payloadVersion} unsupported.`
            + `Expected v${REQUIRED_PAYLOAD_VERSION}`
        );
    }
}

async function verifyEndpointAndGetEndpointDetail(userId, endpointId) {
    /*
    If a directive (other than discovery) is received for a specific endpoint, 
    there are two ways the endpoint may be invalid: Alexa Cloud may think it is
    mapped to our specific user but it has since been reassigned to another user, 
    or, the mapping is correct but, for whatever reason, the "Thing" in AWS IoT
    no longer exists. In the former, an example scenario is that the device was
    given to a friend and that friend registered it to their account. The Alexa
    Cloud does not know this happened... we do not want the original owner to be
    able to control the device at this point, so we should be sure to verify the
    device is associated to the user invoking the skill. In the latter example, 
    it wouldn't be typical to delete an IoT Thing for a given device but its not
    impossible (maybe by mistake, or maybe because a device was suspected as being
    compromised and we want to remove it from service?).
    */
    
    // First, let's see if device is mapped to our user in DynamoDB:
    log('Verifying that endpoint is mapped to the user invoking the skill...')  //vinay
    /*
    var params = {
        Key: {
            hashId: 'userId_' + userId,        
            sortId: 'thingName_' + endpointId
        }, 
        TableName: DEVICE_TABLE_NAME
    };
    log('Calling dynamodb.getItem() with params:', params);
    var getResponse = await dynamodb.get(params).promise();
    */
    var params = {
        ExpressionAttributeValues: {
            ":userid": userId
        }, 
        ProjectionAttributes: "userid",
        KeyConditionExpression: "userid = :userid", 
        TableName: DEVICE_TABLE_NAME
    };
    
    var query_response = await dynamodb.query(params).promise();
    

    var ownlist = (JSON.parse(query_response.Items[0].Own))
    var sharelist = (JSON.parse(query_response.Items[0].Own))
    var devlist = ownlist.list
    devlist.concat(sharelist.list)
    
    if (devlist.indexOf(endpointId) > -1) {
        // The Item key will only be present if the item exists.
        //log("Endpoint is mapped to invoking user.");    //vinay
    }
    else {
        throw new AlexaException(
            'NO_SUCH_ENDPOINT',
            'Endpoint ID exists in AWS IoT Registry but is not mapped to this user in DynamoDB'
        );
    }

    // Now, let's see if the device actually exists in the Iot Core registry.
    // If it does exist, we will populate our response with the IoT thing details
    // and current reported shadow state. 
    var endpoint = {};    
    try {
        // The IoT describeThing() API will throw an error if the given thing
        // name does not exist, so we must wrap in a try/catch block.
        console.log('Verifying existence of endpoint in AWS IoT Registry...');
        console.log(endpointId);    //PTAC
        //vinay: PTAC
        var thngGrp = 'Gateway-' + endpointId.split('-')[1];
        var paramsThing = {
	    "thingGroupName": thngGrp,
	    "maxResults": 2
        };
	    var dev = await iot.listThingsInThingGroup(paramsThing).promise();
	    //Extract PT868 index:
        var thngSearch = endpointId + '-SAUPTZ1PT868-0000000000000000';
        var idx = dev.things.indexOf(thngSearch)
        console.log('PT868 index : ' + idx);

	    console.log(dev.things[idx]); //PT868 model
	    var thng = endpointId;
        endpointId = dev.things[idx];
    
        let params = {
            thingName: endpointId
        };
//        log('Calling iot.describeThing() with params:', params);
        endpoint = await iot.describeThing(params).promise();
//        log("Endpoint exists in Iot Registry");
//        endpoint.config = getDeviceConfigFromIotThingShadow(endpoint.attributes);
        
        // Endpoint ID is core concept when dealing with the Alexa Smart Home API;
        // We happen to use the IoT Registry's thingName as our endpointID, but
        // its possible other implementations may use a different value for the
        // endpoint ID. So, rather than let all later code refer to "thingName",
        // I'd rather explicitly create an endpointId key. That way, it's easier
        // to drop in a different value if you prefer not to use the IoT thing name.
        endpoint.endpointId = endpoint.thingName;
        endpoint.modeldescription = await getModelDescriptionbyModelID(endpoint.thingName.split("-")[2])   //SAUPTZ1PT868 [0]->[2](In future)
        //extract actual thing from Gateway thing Group
        endpoint.shadow = await getDeviceShadow(endpoint.thingName);
        //20210120: HABITAT-113: test gateway shadow
        var shadowGW = await getDeviceShadow(thng);
        var reportstateGW = shadowGW.state.reported;
        var reportpropertiesGW = reportstateGW['000000000001'].properties;
        console.log(reportpropertiesGW['ep0:sGateway:DeviceName']);
        var devName = reportpropertiesGW['ep0:sGateway:DeviceName'];
		
        endpoint.config = getDeviceConfigFromIotThingShadow(endpoint);
        console.log('BEFORE: ' + endpoint.config.friendlyName);
        endpoint.config.friendlyName = devName;
        console.log('AFTER: ' + endpoint.config.friendlyName);
		
//        log('Full endpoint detail:', endpoint);
    }
    catch (err) {
        if (err.name === "ResourceNotFoundException") {
            throw new AlexaException(
                'NO_SUCH_ENDPOINT',
                'Endpoint ID does not exist as Thing Name in AWS IoT Registry'
            );
        }
        else {
            // If it's not a ResourceNotFound error, then it is an unexpected
            // error and we simply pass it upstream to our main error handler.
            throw(err); 
        }
    }
    return endpoint;
}

async function handleDiscovery(request, context, userId) {

    try {
        log("Calling handleDiscovery()");
    
        var alexaResponse = new AlexaResponse({
            "namespace": "Alexa.Discovery",
            "name": "Discover.Response"
            //"name": "AddOrUpdateReport"   //vinay
        });

        var endpoints = await getUserEndpoints(userId);

        endpoints.forEach(endpoint => {
            alexaResponse.addPayloadEndpoint(endpoint);
        });

        return alexaResponse;
    }
    catch (err) {
        throw new Error("handleDiscovery() failed: " + err);
    }

}

async function handleAuthorization(request, context, userId) {

    try {
        log("Calling handleAuthorization()");
    
        var alexaResponse = new AlexaResponse({
            "namespace": "Alexa.Authorization",
            "name": "AcceptGrant.Response"
        });
        
        //For AddOrUpdateReport, may need to capture bearer token and Authorisation code here. Use code like
        //grant_type=authorization_code,code= along with client id and client secret from permission page of skill. 
        //send http post to //https://api.amazon.com/auth/o2/token with those parameters to retrieve access token 
        //and refresh token. Store in DynamoDB inorder to access later while sending event.
        //NOTE: Use refresh token to get new access token. Like http post to https://api.amazon.com/auth/o2/token
        //with grant_type=refresh_token,refresh_token,client_id and client_secret.
        
        //Can test below code by enabling to see if access token and refresh tokens are received or not.
        /*
        var payload = request.directive.payload
        var authCode = payload.grant.code
        console.log('authCode: ', authCode);
        const data = JSON.stringify({});
        
        const options = {
        hostname: 'api.amazon.com',  
        path: '/auth/o2/token',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
            //'Content-Type': 'application/json',
            'grant_type': 'authorization_code',
            'code': authCode,	//'ANPstdRVAPalulehAfEH',
            'client_id':'amzn1.application-oa2-client.3108a5ab50ac4b278c3748f19c4bd020',
            'client_secret': '86a0f2343d2bf279ab4abbefb79faba63733c9341144574344f077cb98ea067a'
        }
        };

        const req = https.request(options, (res) => {
        let data = '';
            console.log('Status Code:', res.statusCode);
        res.on('data', (chunk) => {
            data += chunk;
        });
        res.on('end', () => {
            console.log('Body: ', JSON.parse(data));
        });
        }).on("error", (err) => {
            console.log("Error: ", err.message);
        });
        
        //req.write(data);
        req.end();
        */
        return alexaResponse;
    }
    catch (err) {
        throw new Error("handleAuthorization() failed: " + err);
    }

}

async function verifyAuthTokenAndGetUserId(namespace, directive, ignoreExpiredToken) {
    /* This function calls another function to determine whether the auth token 
       provided by Alexa during invocation is valid and not expired.  
    */
    
    try {
        //log('Validating auth token...');    //vinay
        // The location of the user's auth token is in the payload of the 
        // request directive if the namespace is Discovery; otherwise, it is
        // in the request endpoint:
        var encodedAuthToken;
        //vinay
        var user;
        
        if (namespace === 'Alexa.Discovery') {
            encodedAuthToken = directive.payload.scope.token;
            //vinay
            //console.log('Received token under discovery request.......');   //vinay
            accessToken = directive.payload.scope.token;
        }
        else if (namespace === 'Alexa.Authorization') {
            encodedAuthToken = directive.payload.grantee.token;
        }
        else {
            encodedAuthToken = directive.endpoint.scope.token;
        }


        var params = {
            FunctionName: VERIFY_COGNITO_TOKEN_FUNCTION, 
            InvocationType: "RequestResponse", 
            Payload: JSON.stringify({ token: encodedAuthToken })
        };
    
        //log('Calling lambda.invoke() with params:', params);    //vinay
        var response = await lambda.invoke(params).promise();
        //console.log('Lambda Invoke response.....'+ response.Payload);     //vinay

        if (response.hasOwnProperty('FunctionError')) {
            var authError = JSON.parse(response.Payload);
            log('authError: ', authError)
            // Jason
            if (authError.errorMessage === 'Token is expired' ) {
//                 && ignoreExpiredToken === true) {
                throw new AlexaException(
                    'EXPIRED_AUTHORIZATION_CREDENTIAL',
                    'Auth token is expired'
                );
            }
            else {
                throw new AlexaException(
                    'INVALID_AUTHORIZATION_CREDENTIAL',
                    'Auth token is invalid....'
                );
            }
        }
        //console.log('Auth token is valid...');    //vinay
        var plaintextAuthToken = JSON.parse(response.Payload);
        //vinay
        //console.log('Lambda Invoke response.....'+ plaintextAuthToken.username);  //vinay
        
        // Cognito has both a username and a sub; a sub is unique and never
        // reasigned; a username can be reasigned; it is therefore important
        // to use the 'sub' and not the username as the User ID:
//        var userId = plaintextAuthToken.sub;
        var userId = getUserIdbyUserName(plaintextAuthToken.username);        
        //console.log(userId);  //vinay
        return userId;
    }
    catch (err) {
        console.log(`Error invoking ${VERIFY_COGNITO_TOKEN_FUNCTION}: ${err}`);
        throw (err);
    }
}

// Jason
async function getUserIdbyUserName(userName) {
	var user = userName;
    //Fetch info from DynamoDB
	var params = {
		  TableName: "UserToDeviceList",
		  IndexName: "UserName-index",
		  KeyConditionExpression: 'UserName = :username',
		  ExpressionAttributeValues: {
	   	  ':username' : user
		  }
	   }
   var userdata = await dynamodb.query(params).promise();
   //console.log(userdata);     //vinay
    var userid = userdata.Items[0].userid;
    //console.log('Received User ID....'+ userid);  //vinay
    return userid;
}

async function getUserEndpoints(userId) {
    /*
    This function takes a user ID obtained from the validated and not-expired auth
    token provided by Alexa in the function request and invokes another function
    that returns a list of all devices associated to that user. 
    */
    //log("Getting user endpoints...");     //vinay
    var payload = JSON.stringify({
        userId: userId
    });

    var params = {
        FunctionName: GET_DEVICES_BY_USER_FUNCTION, 
        InvocationType: "RequestResponse", 
        Payload: payload
    };

    //log("Invoking Lambda with params: ", params); //vinay
    var getUserDevicesResponse = await lambda.invoke(params).promise();

    //log("User Device Lambda response: ", getUserDevicesResponse); //vinay
    var devices = (JSON.parse(getUserDevicesResponse.Payload)).deviceList;
    //log("Devices: ", devices);    //vinay
    /*
        response will contain: {
            thingName: "xxxx",
            userId: "yyyy"
        }
    */

    var endpoints = [];

    for (const device of devices) {
        console.log(device);    //vinay
        //PTAC: get thingName from thing Group
        var thngGrp = 'Gateway-' + device.thingName.split('-')[1];
        var paramsThing = {
        	//"thingGroupName": 'Gateway-001E5E02C514',
	        "thingGroupName": thngGrp,
	        "maxResults": 2
        };
	    var dev = await iot.listThingsInThingGroup(paramsThing).promise();
	    //Extract PT868 index:
        var thngSearch = device.thingName + '-SAUPTZ1PT868-0000000000000000';
        var idx = dev.things.indexOf(thngSearch)
        console.log('PT868 index : ' + idx);
	    
	    console.log(dev.things[idx]); //PT868 model
	    var thngName = device.thingName;
        device.thingName = dev.things[idx];
        
        let params = {
                thingName: device.thingName
        };

        //log("Calling IoT.describeThing() with params: ", params); //vinay
        var iotDescription = await iot.describeThing(params).promise();
        iotDescription.endpointId = thngName;//device.thingName;
        iotDescription.modeldescription = device;
        iotDescription.shadow = await getDeviceShadow(device.thingName);
//        log("IoT Description:\n", iotDescription);
        var thingConfig = getDeviceConfigFromIotThingShadow(iotDescription);
        
        //20210118: 
        console.log('Device Name: ' + thingConfig.friendlyName);
        //20210120: HABITAT-113: Device Name using thing Id without PT868 
        var shadowGW = await getDeviceShadow(thngName);
        var reportstateGW = shadowGW.state.reported;
        var reportpropertiesGW = reportstateGW['000000000001'].properties;
        console.log(reportpropertiesGW['ep0:sGateway:DeviceName']);
        var devName = reportpropertiesGW['ep0:sGateway:DeviceName'];
		
        var endpoint = {
            endpointId: thngName,//device.thingName,
            manufacturerName: thingConfig.manufacturerName,
            friendlyName: devName, //thingConfig.friendlyName,	//HABITAT-113
            description: thingConfig.description,
            displayCategories: thingConfig.displayCategories,
            capabilities: thingConfig.capabilities,
        };

        endpoints.push(endpoint);
    }
    
    //log("User endpoints:\n", endpoints);  //vinay
    return endpoints;
}

async function getModelDescriptionbyModelID(modelID) {
	var model = modelID;
    //Fetch info from DynamoDB
	var params = {
		  TableName: MODEL_TABLE_NAME,
		  KeyConditionExpression: 'ModelID = :ModelID',
		  ExpressionAttributeValues: {
	   	  ':ModelID' : model
		  }
	   }
   var modeldescription = await dynamodb.query(params).promise();
   console.log(modeldescription.Items[0]);

    return modeldescription.Items[0];
}

function getDeviceConfigFromIotThingShadow(attributes) {
    var result = {};

//    console.log(attributes)

    var reportstate = attributes.shadow.state.reported;
    
//    console.log("reportstate \n", reportstate)
//    console.log("attributes.modeldescription.nodeID \n",attributes.modeldescription.nodeID)
    //20210118: removed .properties
    var reportproperties = reportstate['000000000003'];/.properties;//reportstate[attributes.modeldescription.nodeID].properties;
    var propertiesprefix = "ep0";//attributes.modeldescription.NodeEndpoint;
    
    result.thingName = attributes.thingName;
    result.manufacturerName = attributes.modeldescription.manufacturerName;
    result.friendlyName = reportproperties["sGateway:DeviceName"];   //need check for PTAC, 20210118:no 'ep0:'
    result.description = attributes.modeldescription.deviceType;
    result.displayCategories = attributes.modeldescription.AlexaDisplayCategories;
    result.capabilities = attributes.modeldescription.AlexaCapabilities;
    
    console.log('getDeviceConfigFromIotThingShadow:' + result);//vinay

    return result;
}

function log(message1, message2) {
    if (message2 == null) {
        console.log(message1);
    } else {
        console.log(message1 + JSON.stringify(message2, null, 2));
    }
}

function validateSetpointIsInAllowedRange(setpoint, validRange) {

    // We convert to the minValue's scale; we don't bother to check whether the min and max are different
    // scales in the discoveryConfig.js file; it would be odd to have them different. 
    let minValue = validRange.minimumValue.value;
    let maxValue = validRange.maximumValue.value;
    let validScale = validRange.minimumValue.scale;
    let desiredValue = convertTemperature(setpoint.value, setpoint.scale, validScale);
    
    if (minValue <= desiredValue && desiredValue <= maxValue) {
        //log(`Requested setpoint of ${desiredValue} within allowed range of ${minValue} to ${maxValue} ${validScale}.`);   //vinay
        return;
    }
    else {
        var errorPayload = {
            validRange: validRange
        };
        throw new AlexaException(
            'TEMPERATURE_VALUE_OUT_OF_RANGE',
            `Requested setpoint of ${desiredValue} outside allowed range of ${minValue} to ${maxValue} ${validScale}.`,
            errorPayload
        );
    }

}

async function handleReportState(request, context, endpoint) {
    
    log('Gathering state from IoT Thing shadow to report back to Alexa...');
    //vinay
    console.log(endpoint.config);
    console.log(endpoint.config.thingName); //check for PTAC thing name
    var userId = endpoint.config.thingName
    var devName = endpoint.config.friendlyName
    var msgId = request.directive.header.messageId
    
    console.log(request.directive);
    console.log(msgId);
    
    console.log(endpoint.endpointId);
    var endpointId = endpoint.endpointId;
    var reportstate = endpoint.shadow.state.reported;
    var reportproperties = reportstate['000000000003'].properties; //reportstate[endpoint.modeldescription.nodeID].properties;
    var propertiesprefix = "ep0";//endpoint.modeldescription.NodeEndpoint;
    
    var token = request.directive.endpoint.scope.token;
    var correlationToken = request.directive.header.correlationToken;
    var currentState = reportproperties;   
    
    var heatingsetpoint = reportproperties[propertiesprefix + ":sPTAC868:HeatingSetpoint_x100"];
    var coolingsetpoint = reportproperties[propertiesprefix + ":sPTAC868:CoolingSetpoint_x100"];
    var systemmode = reportproperties[propertiesprefix + ":sPTAC868:SystemMode"];
    var scale = reportproperties[propertiesprefix + ":sPTAC868:TemperatureDisplayMode"];
    var temperature = reportproperties[propertiesprefix + ":sPTAC868:LocalTemperature_x100"];
    var runningmode = reportproperties[propertiesprefix + ":sPTAC868:RunningMode"];
    //PTAC online check ??
    var connectivity = 1; //reportproperties[propertiesprefix + ":sGateway:CloudConnected_i"];
    //var connectivity = reportstate.connected;

    //vinay
    //var currdevName = reportproperties["ep0:sGateway:DeviceName"];
    //console.log('user id: '+ userId + 'synced device name' + devName + 'current device name' + currdevName); 
    console.log(request.directive.endpoint.scope.token)

    /*
    const postmanToken = 'eyJraWQiOiJJQUFXazl2K3FFaXNNQ1F2NGlsK0NRK0xGbEY2UGlwaTl0VGQxcE9BamMwPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiIyMDMwOTI4Ny02MGQ2LTRlN2ItOWIzMy0yYzQyNTExY2M0NDQiLCJldmVudF9pZCI6IjUwN2RhZGNiLTRkYTEtNGVjNC1hYzI1LTg3ZDJmNDkzYThkZSIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE2MDMzMTEyNTQsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC51cy13ZXN0LTIuYW1hem9uYXdzLmNvbVwvdXMtd2VzdC0yX2xiNVpFQ3RSaSIsImV4cCI6MTYwMzMxNDg1NCwiaWF0IjoxNjAzMzExMjU0LCJ2ZXJzaW9uIjoyLCJqdGkiOiIxNDcwZTQyZi1iZTZlLTRjMzctYjJmNS01MzBmYmFlM2VmN2EiLCJjbGllbnRfaWQiOiI0amJvOW4wcDE3MjUwbXFqc2Z1aWszbGxsdSIsInVzZXJuYW1lIjoiamFzb25jaGFuLmRldmVsb3BlckBnbWFpbC5jb20ifQ.HNLmq8XwuOR-Ndz3FEnKa-A8zwHuLxi6l8w3l52nZIi_Ij9rZLE_GKw_JyTua2QhNv0_txfg9pZQ99tYm_7Cg2WHmMRvR2XqYuHOqaKUch5nsetXrbFm4XfYiZdCcDTJNQzUgXYCv_FbxHiItExxKGLAeybBLo8ljUEAOs2fVMXhJOmUmJvFuEoXa1jmjX8714T27sfDeLRcy5hATXmhZnHwxyBHu47EUox7l7XjOjQoLDjp425HO1GciigG-SUVm3aZ6rU_EgDHFW9eoWhOaPWNBYax3XiQEc9XtIcpOJHE0sDHPrgt0t2RWD-ED59AXtvm_ti1CMkG48yMSu8GpA';

    const epDel = {
    event:{}
    }
    epDel.event = {
        header: {
            messageId: msgId,
            name: "DeleteReport",
            namespace: "Alexa.Discovery",
            payloadVersion: 3
        },
        payload:{
            endpoints:[{
                endpointId: endpointId    
            }
            ],
            scope:{
                type: "Bearer token",
                token: token
            }    
        }
    }
    const epDelete = {
    event: {
        header: {
          messageId: msgId,
          name: "DeleteReport",
          namespace: "Alexa.Discovery",
          payloadVersion: 3
        },
        payload: {
          endpoints: [{
              endpointId: userId
            }
          ],
          scope: {
            type: "BearerToken",
            token: token
          }
        }
    }
    }
    
    var endpointUpdate = {
  "event": {
    "header": {
      "namespace": "Alexa.Discovery",
      "name": "AddOrUpdateReport",
      "payloadVersion": "3",
      "messageId": msgId
    },
    "payload": {
      "endpoints": [
        {
          "endpointId": userId,
          "manufacturerName": "Computime",
          "description": "WiFi Thermostat",
          "friendlyName": "Ktchn",
          "additionalAttributes":  {
            "manufacturer" : "Salus",
            "model" : "ST921WF",
            "serialNumber": "001E5E02F6F8",
            "firmwareVersion" : "2.0",
            "softwareVersion": "1.0",
            "customIdentifier": "00"
          },
          "displayCategories": [
            { "S" : "THERMOSTAT" },  { "S" : "TEMPERATURE_SENSOR" }
          ],
          "capabilities": [
		  { "M" : {  "interface" : { "S" : "Alexa.EndpointHealth" },  "properties" : { "M" : {   "proactivelyReported" : { "BOOL" : true },   "retrievable" : { "BOOL" : true }, 
		  "supported" : { "L" : [  { "M" : {   "name" : { "S" : "connectivity" }  }  }  ]  }  } },  "type" : { "S" : "AlexaInterface" },    
		  "version" : { "S" : "3" }    }  },  { "M" : {  "interface" : { "S" : "Alexa.TemperatureSensor" },  "properties" : { "M" : {   "proactivelyReported" : { "BOOL" : true }, 
		  "retrievable" : { "BOOL" : true },  "supported" : { "L" : [  { "M" : {    "name" : { "S" : "temperature" }  }   }  ]  }  }  }, 
		  "type" : { "S" : "AlexaInterface" },  "version" : { "S" : "3" }    }  },  { "M" : { "configuration" : { "M" : {  "supportedModes" : { "L" : [ { "S" : "HEAT" },    { "S" : "COOL" },  { "S" : "AUTO" }, { "S" : "OFF" }  ]   },
          "supportsScheduling" : { "BOOL" : false } } },  "interface" : { "S" : "Alexa.ThermostatController" }, "properties" : { "M" : {  "proactivelyReported" : { "BOOL" : true }, "retrievable" : { "BOOL" : true },          
		  "supported" : { "L" : [   { "M" : {   "name" : { "S" : "thermostatMode" }  } },  { "M" : {    "name" : { "S" : "targetSetpoint" }  }  }  ] } }  },  
		  "type" : { "S" : "AlexaInterface" }, "version" : { "S" : "3" }    }  },  { "M" : {      "interface" : { "S" : "Alexa.TemperatureSensor" },     
		  "properties" : { "M" : {  "proactivelyReported" : { "BOOL" : true }, "retrievable" : { "BOOL" : true }, "supported" : { "L" : [  { "M" : { 
		  "name" : { "S" : "temperature" }  }  }   ]  }  }  }, "type" : { "S" : "AlexaInterface" }, "version" : { "S" : "3" }    }  },  { "M" : {   "interface" : { "S" : "Alexa" },   "type" : { "S" : "AlexaInterface" },    
		  "version" : { "S" : "3" } }  }
          ],
          "connections": [
          ],
          "cookie": {
          }
        }
      ],
      "scope": {
        "type": "BearerToken",
        "token": token
      }
    }
  }
};

    console.log(epDel)
    const data = JSON.stringify(epDel);
    var buff = new Buffer.from(token);
    var base64token = buff.toString('base64');
    const options = {
    //hostname: 'reqres.in', 
    //path: '/api/users', 
    hostname: 'api.amazonalexa.com',    //N. Virginia   fe->US west Oregon, eu->EU Ireland 
    path: '/v3/events',
    method: 'POST',
    headers: {
        //'Authorization': 'Bearer Atza|' + base64token + '',
        'Authorization': '', //access token obtained from refresh token
        'Content-Type': 'application/json',
        'Content-Length': data.length
        }
    };
    
    const req = https.request(options, (res) => {
    let data = '';
    console.log('Status Code:', res.statusCode);

    res.on('data', (chunk) => {
        data += chunk;
    });

    res.on('end', () => {
        console.log('Body: ', JSON.parse(data));
    });

    }).on("error", (err) => {
    console.log("Error: ", err.message);
    });

    req.write(data);
    req.end();
    */

    var reportdata = { }
    //PTAC
    //if (connectivity == 'true')
    if (connectivity == 1)
        reportdata[connectivity] = "OK"
    else
        reportdata[connectivity] = "FAIL" // FIXME

    //vinay debug
    //reportdata[scale] = "CELSIUS"     //shows in F for Toilet device
    //vinay debug 
    if (reportdata[scale] == "FAHRENHEIT")
    {
        reportdata[temperature] = temperature / 100
        reportdata[heatingsetpoint] = heatingsetpoint / 100
        reportdata[coolingsetpoint] = coolingsetpoint / 100
    }
    else
    {
        reportdata[temperature] = convertTemperature(temperature/100 , "CELSIUS", "FAHRENHEIT")
        reportdata[heatingsetpoint] = convertTemperature(heatingsetpoint/100 , "CELSIUS", "FAHRENHEIT")
        reportdata[coolingsetpoint] = convertTemperature(coolingsetpoint/100 , "CELSIUS", "FAHRENHEIT")
    }
    
    //20210118
    console.log('Room temperature: ', reportdata[temperature]);

    switch(systemmode) {
        case 0:
            reportdata[systemmode] = "OFF"
            break
        case 1:
            reportdata[systemmode] = "AUTO"
            break
        case 2:
            reportdata[systemmode] = "COOL"
            break        
        case 3:
            reportdata[systemmode] = "COOL"
            break
        case 4:
            reportdata[systemmode] = "HEAT"
            break    
    }

    
    // Basic response header
    var alexaResponse = new AlexaResponse(
        {
            "name": 'StateReport',
            //"name": 'AddOrUpdateReport',  //vinay
            "correlationToken": correlationToken,
            "token": token,
            "endpointId": endpointId
        }
    );

    // Gather current properties and add to our response
    alexaResponse.addContextProperty({
        namespace: "Alexa.EndpointHealth",
        name: "connectivity",
        value: {
            value: reportdata[connectivity]
        } 
    });


    if (systemmode == 3) 
    {
        alexaResponse.addContextProperty({
            namespace: "Alexa.ThermostatController",
            name: "targetSetpoint",
            value: {
                value: reportdata[coolingsetpoint],
                //scale: reportdata[scale]
                scale: "FAHRENHEIT"  
            }
        });
    }
    else if (systemmode == 4)
    {
        alexaResponse.addContextProperty({
            namespace: "Alexa.ThermostatController",
            name: "targetSetpoint",
            value: {
                value: reportdata[heatingsetpoint],
                //scale: reportdata[scale]
                scale: "FAHRENHEIT"  
            }
        });
    }
    else if (systemmode == 1)
    {
        var autosetpoint = 5
        if (runningmode == 3)
        {
            autosetpoint = reportdata[coolingsetpoint]
        }
        else if  (runningmode == 4)
        {
            autosetpoint = reportdata[heatingsetpoint]
        }
        alexaResponse.addContextProperty({
            namespace: "Alexa.ThermostatController",
            name: "targetSetpoint",
            value: {
                value: autosetpoint,
                //vinay debug remove later
                scale: "FAHRENHEIT"     //shows in C for Kitchen device
                //scale: reportdata[scale]
            }
        });
    }
        
    alexaResponse.addContextProperty({
        namespace: "Alexa.ThermostatController",
        name: "thermostatMode",
        value: reportdata[systemmode]
    });

    alexaResponse.addContextProperty({
        namespace: "Alexa.TemperatureSensor",
        name: "temperature",
        //vinay
        //value: reportdata[temperature]  
        value: {
                value: reportdata[temperature],
                //scale: reportdata[scale]
                scale: "FAHRENHEIT"
            }
    });
    
    //vinay
    /*alexaResponse.addContextProperty({
        namespace: "Alexa.Discovery",
        name: "AddOrUpdateReport",
        endpoints: endpointUpdate 
    });*/

    return alexaResponse.get();
}

async function handleThermostatControl(request, context, endpoint) {
    /*  This function handles all requests that Alexa identifies as being a 
        "ThermostatController" directive, such as:
          - Turn device to cool mode
          - Turn device to heat mode
          - Increase device temperature
          - Decrease device temperature
          - Set temperature to X degrees
    */
    console.log('handleThermostatControl.......' + endpoint.endpointId);   //PTAC
    var endpointId = endpoint.endpointId;
    var thingName = endpoint.thingName;
    var token = request.directive.endpoint.scope.token;
    var correlationToken = request.directive.header.correlationToken;
    var requestMethod = request.directive.header.name; 
    var payload = request.directive.payload;
    //vinay
    var msgId = request.directive.header.messageId;
    var errResponse = 0;
    
    var alexaResponse = new AlexaResponse(
        {
            "correlationToken": correlationToken,
            "token": token,
            "endpointId": endpointId
        }
    );
    
        
    //vinay    
        //validateSetpointIsInAllowedRange(targetSetpoint, endpoint.config.validRange);
    var reportstate = endpoint.shadow.state.reported;
    var propertiesprefix = "ep0";//endpoint.modeldescription.NodeEndpoint;
    var nodeid = '000000000003';//endpoint.modeldescription.nodeID
    var reportproperties = reportstate[nodeid].properties;//reportstate[endpoint.modeldescription.nodeID].properties;
    var minheatsetpoint = reportproperties[propertiesprefix + ":sPTAC868:MinHeatingSetpoint_x100"];
    var maxheatsetpoint = reportproperties[propertiesprefix + ":sPTAC868:MaxHeatingSetpoint_x100"];
    var mincoolsetpoint = reportproperties[propertiesprefix + ":sPTAC868:MinCoolingSetpoint_x100"];
    var maxcoolsetpoint = reportproperties[propertiesprefix + ":sPTAC868:MaxCoolingSetpoint_x100"];	
    var scale = reportproperties[propertiesprefix + ":sPTAC868:TemperatureDisplayMode"];
    var runningmode = reportproperties[propertiesprefix + ":sPTAC868:RunningMode"];
    var systemmode = reportproperties[propertiesprefix + ":sPTAC868:SystemMode"];
    var heatingsetpoint = reportproperties[propertiesprefix + ":sPTAC868:HeatingSetpoint_x100"];
    var coolingsetpoint = reportproperties[propertiesprefix + ":sPTAC868:CoolingSetpoint_x100"];
    
    var desireddata = {}
    var desireproperties = {}
    
    //log(`Running ThermostatControl handler for ${requestMethod} method`);     //vinay
    console.log('Min CoolingSetpoint_x100, Max CoolingSetpoint_x100,........'+ mincoolsetpoint, maxcoolsetpoint);
    console.log('Min HeatingSetpoint_x100, Max HeatingSetpoint_x100,........'+ minheatsetpoint, maxheatsetpoint);
    
    if (requestMethod === 'SetTargetTemperature') {

        var targetSetpoint = payload.targetSetpoint;
        desireddata.targetsetpoint = 0
        
        if (payload.hasOwnProperty("targetSetpoint"))
        {
            if (payload.targetSetpoint.scale == "CELSIUS")
            {
        		desireddata.targetsetpoint = targetSetpoint.value * 100;
            }
            else
            {
                desireddata.targetsetpoint = convertTemperature(targetSetpoint.value , "FAHRENHEIT", "CELSIUS") * 100
            }
        }
    	desireddata.targetsetpoint = Math.round(desireddata.targetsetpoint)

        if ((systemmode == 3) || (systemmode == 1 && runningmode == 3))
        {
    		if(desireddata.targetsetpoint < mincoolsetpoint || desireddata.targetsetpoint > maxcoolsetpoint)
    		{
    		    errResponse = 1;
           	    /*var errorContextProperty = {
                namespace: "Alexa.ThermostatController",
                name: "ErrorResponse",
                type: "TEMPERATURE_VALUE_OUT_OF_RANGE",
                message: "setpoint outside allowed range",
                validRange: {
                        minimumValue: {
                                value: 10.0,
                                scale: "CELSIUS"
                                },
                        maximumValue: {
                                value: 37.0,
                                scale: "CELSIUS"
                        }
                    }
                };
                alexaResponse.addContextProperty(errorContextProperty);
                return alexaResponse.get(); 
                */
    	        var errorPayload = {
                validRange: {
                        minimumValue: {
                                value: 10.0,    //50F
                                scale: "CELSIUS"
                                },
                        maximumValue: {
                                value: 37.0,    //99F
                                scale: "CELSIUS"
                        }
                    }
    			};
    			throw new AlexaException(
    				'TEMPERATURE_VALUE_OUT_OF_RANGE',
    				`Requested target setpoint  outside allowed range of ${mincoolsetpoint} to ${maxcoolsetpoint}.`,
    				errorPayload
    			);
    		}
    		
    		desireproperties[propertiesprefix + ":sPTAC868:SetCoolingSetpoint_x100"] = desireddata.targetsetpoint
    	}
    	else if (((systemmode == 4) || (systemmode == 5)) || (systemmode == 1 && runningmode == 4))
    	{	
    	    console.log('Target Setpoint Validation.....'+ desireddata.targetsetpoint + minheatsetpoint + maxheatsetpoint);
    	    //vinay: change to || not &&
    		if(desireddata.targetsetpoint < minheatsetpoint || desireddata.targetsetpoint > maxheatsetpoint)
    		{
    		    errResponse = 1;
        	    console.log('Target Setpoint Validation Started........'+ errResponse);
        	    /*var errorContextProperty = {
                namespace: "Alexa.ThermostatController",
                name: "ErrorResponse",
                type: "THERMOSTAT_IS_OFF", //"TEMPERATURE_VALUE_OUT_OF_RANGE",
                message: "The thermostat is currently off." //"setpoint outside allowed range2",
                validRange: {
                        minimumValue: {
                                value: 5.0,
                                scale: "CELSIUS"
                                },
                        maximumValue: {
                                value: 32.0,
                                scale: "CELSIUS"
                        }
                    }
                };
                alexaResponse.addContextProperty(errorContextProperty);
                return alexaResponse.get();*/
    	        var errorPayload = {
                validRange: {
                        minimumValue: {
                                value: 5.0,     //41F
                                scale: "CELSIUS"
                                },
                        maximumValue: {
                                value: 32.0,    //90F
                                scale: "CELSIUS"
                        }
                    }
    			};
    			throw new AlexaException(
    				'TEMPERATURE_VALUE_OUT_OF_RANGE',
    				`Requested target setpoint outside allowed range of ${minheatsetpoint} to ${maxheatsetpoint}.`,
    				errorPayload
    			);
    		}
    		desireproperties[propertiesprefix + ":sPTAC868:SetHeatingSetpoint_x100"] = desireddata.targetsetpoint
    	}
    	//20210118: Add error response for Off mode, Yet to test
    	else if(systemmode == 0)
    	{
    	    var errorPayload = {
                currentDeviceMode: "OFF"
    			};

    	    /*throw new AlexaException(
            'NOT_SUPPORTED_IN_CURRENT_MODE',
            'Thermostat is Off, Please turn it On', errorPayload
            );*/
    	    /*throw new AlexaException(
            'THERMOSTAT_IS_OFF',
            'The thermostat is currently off.'//, errorPayload
            );*/
            var alexaErrResp = new AlexaResponse(
                {
                event: 
                {
                header: {
                namespace: "Alexa.ThermostatController",
                name: "ErrorResponse",
                messageId: msgId,
                correlationToken: correlationToken,
                payloadVersion: "3"
                },
                endpoint:{
                scope:{
                    type: "BearerToken",
                    token: token
                    },
                endpointId: endpointId
                },
                payload: {
                type: "THERMOSTAT_IS_OFF",
                message: "The thermostat is currently off."
                }
                }
                }
            );
            
            return alexaErrResp.get();
    	}
	
        let desiredshadowState = {
            state: {
                //desired: {
                //}
            }
        };
        desiredshadowState.state[nodeid] = {properties: desireproperties}

        //console.log(desiredshadowState)   //vinay

        // For debugging purposes, we may choose to copy the desired state to reported state:
        //if (copyDesiredStateToReportedStateInShadow === true) {
        //    shadowState.state.reported = shadowState.state.desired;
        //}
        // FIXME: Should we ge shadow after get the data?

        await updateThingShadow(thingName, desiredshadowState);

        //vinay, alexa response after successful update
        if(errResponse == 0)
        {
        var targetpointContextProperty = {
            namespace: "Alexa.ThermostatController",
            name: "targetSetpoint",
            value: {
                value: targetSetpoint.value,
                scale: targetSetpoint.scale
            }
        };
        alexaResponse.addContextProperty(targetpointContextProperty);
        return alexaResponse.get();
        }
        
    }
    else if (requestMethod === 'AdjustTargetTemperature') {
//        var currentSetpoint = endpoint.shadow.state.reported.targetSetpoint;
        var currentSetpoint = 5
        if ((systemmode == 2)  || (systemmode == 1 && runningmode == 3))
            currentSetpoint = coolingsetpoint / 100
        else if (((systemmode == 3) || (systemmode == 4))  || (systemmode == 1 && runningmode == 4))
            currentSetpoint = heatingsetpoint / 100
        
        var currentValue = currentSetpoint;
        var currentScale = "CELSIUS";
        
        var targetSetpointDelta = payload.targetSetpointDelta;
        var deltaValue = targetSetpointDelta.value;
        var deltaScale = targetSetpointDelta.scale;

        //log('Current setpoint:', currentSetpoint);    //vinay
        //log('Target delta:', targetSetpointDelta);    //vinay

        // It's possible that the requested temperature change is in a different
        // scale from that being used/reported by the device. In such a case, 
        // the Alexa guide states we should report the new adjusted value in the
        // device's scale. When the scales are different, we must convert the
        // current temperature to the scale of the requested delta in order to
        // add (or subtract) the delta from the current temperature to arrive
        // at the new desired temperature. Then, we must convert this new value
        // back to the temp scale currently in use by the device. If the current
        // and delta scales are the same (e.g. both Fahrenheit or both Celsius),
        // then the convertTemperature() function simply returns the same value
        // it was provided, without modification. 
        // https://developer.amazon.com/docs/device-apis/alexa-thermostatcontroller.html#adjusttargettemperature-directive
        var currentValueInDeltaScale = convertTemperature(currentValue, currentScale, deltaScale);
        var newValueInDeltaScale = currentValueInDeltaScale + deltaValue;
		var newValueInCurrentScale = convertTemperature(newValueInDeltaScale, deltaScale, currentScale);
		//var newValueInCurrentScale = newValueInDeltaScale;
		
		//Debug HABITAT-133, remove math.round
        //newValueInCurrentScale = Math.round(newValueInCurrentScale)
        newValueInCurrentScale = newValueInCurrentScale
        var newTargetSetpoint = {
            value: newValueInCurrentScale,
            scale: currentScale
        };

        if ((systemmode == 3)  || (systemmode == 1 && runningmode == 3))
            desireproperties[propertiesprefix + ":sPTAC868:SetCoolingSetpoint_x100"] = newValueInCurrentScale * 100
        else if (((systemmode == 4) || (systemmode == 5))  || (systemmode == 1 && runningmode == 4))
            desireproperties[propertiesprefix + ":sPTAC868:SetHeatingSetpoint_x100"] = newValueInCurrentScale * 100
		//20210122: HABITAT-142
    	else if(systemmode == 0)
    	{
            var alexaErrResp = new AlexaResponse(
                {
                event: 
                {
                header: {
                namespace: "Alexa.ThermostatController",
                name: "ErrorResponse",
                messageId: msgId,
                correlationToken: correlationToken,
                payloadVersion: "3"
                },
                endpoint:{
                scope:{
                    type: "BearerToken",
                    token: token
                    },
                endpointId: endpointId
                },
                payload: {
                type: "THERMOSTAT_IS_OFF",
                message: "The thermostat is currently off."
                }
                }
                }
            );
            
            return alexaErrResp.get();
    	}

        let desiredshadowState = {
            state: {
                //desired: {
                    
                //}
            }
        };
        desiredshadowState.state[nodeid] = {properties: desireproperties}
        
        // For debugging purposes, we may choose to copy the desired state to reported state:
//        if (copyDesiredStateToReportedStateInShadow === true) {
//            shadowState.state.reported = shadowState.state.desired;
//        }
// FIXME: Should we ge shadow after get the data?

        await updateThingShadow(thingName, desiredshadowState);

        alexaResponse.addContextProperty({
            namespace: "Alexa.ThermostatController",
            name: "targetSetpoint",
            value: newTargetSetpoint
        });

        return alexaResponse.get();

    }
    else if (requestMethod === 'SetThermostatMode') {
        
        var thermostatMode = payload.thermostatMode;
        var targetmode = 0
        switch (thermostatMode.value)
        {
            case "OFF":
                targetmode = 0
                break;
            case "AUTO":
                targetmode = 1
                break;
            case "COOL":
                targetmode = 3
                break;
            case "HEAT":
                targetmode = 4
                break;
            default:
                targetmode = 0
                break;
                
        }

        desireproperties[propertiesprefix + ":sPTAC868:SetSystemMode"] = targetmode
		//if(systemmode !== 0)
		if(targetmode !== 0)	
        desireproperties[propertiesprefix + ":sPTAC868:SetFanMode"] = 5		//20210125: HABITAT-140, 20210101: HABITAT-148: Should check for target mode OFF not current system mode. 			
			
        let desiredshadowState = {
            state: {
                //desired: {
                    
                //}
            }
        };
        desiredshadowState.state[nodeid] = {properties: desireproperties}


        // For debugging purposes, we may choose to copy the desired state to reported state:
//        if (copyDesiredStateToReportedStateInShadow === true) {
//            shadowState.state.reported = shadowState.state.desired;
//        }
        // FIXME: Should we ge shadow after get the data?

        await updateThingShadow(thingName, desiredshadowState);

        // Context properties are how we affirmatively tell Alexa that the state
        // of our device after we have successfully completed the requested changes.
        // The not required, it is recommended that *all* properties be reported back, 
        // regardless of whether they were changed. 
        // At the moment, we are only reporting back the changed property.
        alexaResponse.addContextProperty({
            namespace: "Alexa.ThermostatController",
            name: "thermostatMode",
            value: thermostatMode.value
        });

        return alexaResponse.get();
    }
    else {
        log(`ERROR: Unsupported request method ${requestMethod} for ThermostatController.`);
        throw new AlexaException(
            'INTERNAL_ERROR',
            'Unsupported request method ${requestMethod} for ThermostatController'
        );
    }    
}

function convertTemperature(temperature, currentScale, desiredScale) {
    if (currentScale === desiredScale) {
        return temperature;
    }
    else if (currentScale === 'FAHRENHEIT' && desiredScale === 'CELSIUS') {
        return convertFahrenheitToCelsius(temperature);
    }
    else if (currentScale === 'CELSIUS' && desiredScale === 'FAHRENHEIT') {
        return convertCelsiusToFahrenheit(temperature);
    }
    else {
        throw new Error(`Unable to convert ${currentScale} to ${desiredScale}, unsupported temp scale.`);
    }
    
}

function convertCelsiusToFahrenheit(celsius) {
    var fahrenheit = Math.round(((celsius * 1.8) + 32) * 100);
    log(`Converted temperature to ${fahrenheit} FAHRENHEIT.`);
    //20210118: HABITAT-133: Added Math.trunc
    //return Math.trunc(fahrenheit/100);
    return Math.round(fahrenheit/100);  //works well for both HABITAT-133 and 1 degF error removal
}

function convertFahrenheitToCelsius(fahrenheit) {
    var celsius = Math.round(((fahrenheit - 32) * 0.556)*100);
    //log(`Converted temperature to ${celsius} CELSIUS.`);  //vinay
    //return Math.round(celsius / 100);	//Debug HABITAT-133
	return celsius / 100;	//Debug HABITAT-133
}

async function getDeviceShadow(thingName) {
    // Get the device's reported state per the state.reported object of the
    // corresponding IoT thing's device shadow.
    var params = {
        thingName: thingName
    };
    //log('Calling iotdata.getThingShadow() with params:', params); //vinay
    var response = await iotdata.getThingShadow(params).promise();
    var shadow = JSON.parse(response.payload);
    log('getThingShadow() response:', shadow);
    return shadow;
    
}

async function updateThingShadow(thingName, shadowState) {
    try {
        //var desiredstate = shadowState.state.desired;
        //desiredstate["version"] = 1
        //desiredstate["createdAt"] = new Date().toISOString()
        
        var payloadjson = JSON.stringify(shadowState)
        //console.log(payloadjson)  //vinay
        var sendtopic = "$aws/things/" + thingName + "/shadow/update"
//        payloadjson = '{{"state":{"desired":{"000000000003":{"properties":{"ep0:sWIFITherS:SetHeatingSetpoint_x100":1946}},"version":1,"createdAt":"2020-08-31T01:32:58.735Z"}}}}'
        var params = {
            payload: Buffer.from(payloadjson),
            topic: sendtopic,
            qos: '1'
        };
        // Updating shadow updates our *control plane*, but it doesn't necessarily
        // mean our device state has changed. The device is responsible for monitoring
        // the device shadow and responding to changes in desired states. 
        //log(`Calling iotdata.publish() with params:`, params);    //vinay
        const request = iotdata.publish(params)
        var updateShadowResponse = await request.promise()
//        var updateShadowResponse = await iotdata.updateThingShadow(params).promise();
        //log(`Shadow update response:\n`, (updateShadowResponse));     //vinay  
    }
    catch (err) {
        console.log('Error: unable to update device shadow:', err);
        throw (err);
    }
}

function sendResponse(response) {
//    log("Lambda response to Alexa Cloud:\n", response);
    return response
}

function sendErrorResponse(type, message, additionalPayload) {
    log("Preparing error response to Alexa Cloud...");
    var payload = {
        "type": type,
        "message": message
    };
    // Certain Alexa error response types allow or require additional parameters in the payload response
    if (additionalPayload !== undefined) {
        payload = { ...payload, ...additionalPayload }
    }
    var alexaErrorResponse = new AlexaResponse({
        "name": "ErrorResponse",
        "payload": payload
    });
    return sendResponse(alexaErrorResponse.get());
}

function AlexaException(name, message, additionalPayload) {
    log('Creating handled Alexa exception...');
    // The error name should be one of the Alexa.ErrorResponse interface types:
    // https://developer.amazon.com/docs/device-apis/alexa-errorresponse.html
    var error = new Error(); 
    this.stack = error.stack;
    this.name = name;
    this.message = message;
    this.AlexaError = true;
    this.additionalPayload = checkValue(additionalPayload, undefined);
}

function checkValue(value, defaultValue) {

    if (value === undefined || value === {} || value === "") {
        return defaultValue;
    }
    return value;
}